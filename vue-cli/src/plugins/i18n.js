import Vue from 'vue';
import VueI18n from 'vue-i18n';


// import json from '../copy/websiteCopy.json'
import en from '../copy/enWebsiteCopy.json'
import es from '../copy/esWebsiteCopy.json'
import de from '../copy/deWebsiteCopy.json'
import pt from '../copy/ptbrWebsiteCopy.json'
import ru from '../copy/ruWebsiteCopy.json'

Vue.use(VueI18n);

// let currentLocaleCopy = {}

const messages = {
    "en": en,
    "es": es,
    "de": de,
    "pt": pt,
    "ru": ru
}

const i18n = new VueI18n({
    locale: 'en', // set locale
    fallbackLocale: 'en', // set fallback locale
    pluralizationRules: {
        /**
         * many thanks to https://kazupon.github.io/vue-i18n/guide/pluralization.html#custom-pluralization
         * @param choice {number} a choice index given by the input to $tc: `$tc('path.to.rule', choiceIndex)`
         * @param choicesLength {number} an overall amount of available choices
         * @returns a final choice index to select plural word by
         */
        'ru': function(choice, choicesLength) {
            // this === VueI18n instance, so the locale property also exists here

            if (choice === 0) {
                return 0;
            }

            const teen = choice > 10 && choice < 20;
            const endsWithOne = choice % 10 === 1;

            if (choicesLength < 4) {
                return (!teen && endsWithOne) ? 1 : 2;
            }
            if (!teen && endsWithOne) {
                return 1;
            }
            if (!teen && choice % 10 >= 2 && choice % 10 <= 4) {
                return 2;
            }

            return (choicesLength < 4) ? 2 : 3;
        }
    },
    messages,
});

export default i18n;
