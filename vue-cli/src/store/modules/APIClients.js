import axios from "axios";
import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    createError: null,
    clientFormData: {
      scopes: []
    },
    clientList: [],
    scopeList: [],
  },
  getters: {
    
    GET_CLIENT_FORM_SCOPES: (state) => {
      return state.clientFormData.scopes;
    },
    GET_CLIENTS: (state) => {
      return state.clientList;
    },
    GET_SCOPES: (state) => {
      return state.scopeList;
    },
    GET_CREATE_ERROR: (state) => {
      return state.createError;
    },
  },
  mutations: {
    SET_CLIENT_FORM_SCOPES(state, payload) {
      Vue.set(state.clientFormData, 'scopes', payload);
    },
    SET_CLIENT_LIST(state, payload) {
      state.clientList = payload;
    },
    APPEND_CLIENT_LIST(state, payload) {
      state.clientList.push( payload);
    },
    REMOVE_CLIENT_FROM_LIST(state, payload) {
      let c;
      for (c in state.clientList) {
        if (state.clientList[c].clientId == payload) {
          state.clientList.splice(c, 1);
        }
      }
    },
    SET_SCOPE_LIST(state, payload) {
      state.scopeList = payload;
    },
    SET_CREATE_ERROR(state, payload) {
      state.createError = payload;
    },

  },
  actions: {
    UPDATE_CLIENT_FORM_SCOPES({ commit }, payload) { 
      commit('SET_CLIENT_FORM_SCOPES', payload)
    },
    async FETCH_CLIENTS({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_AUTHAPIBASEURL + "clients/getmine";
      if (!rootGetters["UserStore/userToken"]) {
        console.log("User not logged in");
        return false;
      }
      await axios
        .get(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_CLIENT_LIST", response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    FETCH_SCOPES({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_AUTHAPIBASEURL + "oauth/scopes";
      if (!rootGetters["UserStore/userToken"]) {
        console.log("User not logged in");
        return false;
      }
      axios
        .get(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("SET_SCOPE_LIST", response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async CREATE_CLIENT({ commit, rootGetters, state }) {
      var url;
      url = process.env.VUE_APP_AUTHAPIBASEURL + "clients/create";
      if (!rootGetters["UserStore/userToken"]) {
        console.log("User not logged in");
        return false;
      }
      await axios
        .post(encodeURI(url), { scopes:  state.clientFormData.scopes, flow: "client_credentials"  }, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("APPEND_CLIENT_LIST", response.data);
        })
        .catch(function (error) {
          console.log(error);
          commit("SET_CREATE_ERROR", error);
        });
    },
    async DELETE_CLIENT({ commit, rootGetters }, payload) {
      var url;
      url = process.env.VUE_APP_AUTHAPIBASEURL + "clients/delete/"+payload;
      if (!rootGetters["UserStore/userToken"]) {
        console.log("User not logged in");
        return false;
      }
      await axios
        .delete(encodeURI(url), rootGetters["UserStore/authHeader"])
        .then(function () {
          commit("REMOVE_CLIENT_FROM_LIST", payload);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  },
};
