import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import store from "./store";
import router from "./router";
import VueMeta from 'vue-meta'
import common from "@shared/common";
import i18n from "@/plugins/i18n";
import FlagIcon from "vue-flag-icon";
import "vue-croppa/dist/vue-croppa.css";
import Croppa from "vue-croppa";
import VueAnalytics from "vue-ua";

Vue.use(Croppa);
Vue.use(FlagIcon);
Vue.use(VueMeta);
Vue.config.productionTip = false;
Vue.use(VueAnalytics, {
  appName: "WTS2",
  appVersion: "1.0.0",
  trackingId: window.location.hostname == 'contribute.wt.social' ? process.env.VUE_APP_GA_KEY_CONTRIBUTE : process.env.VUE_APP_GA_KEY,
  debug: false,
  trackPage: true, 
  vueRouter: router,
});

require("@shared/common");
new Vue({
  
  store,
  vuetify,
  router,
  common,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
