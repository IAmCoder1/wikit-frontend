// As a user
// I would like to create an account
// So that I can use WT.Social

// Nightwatch DOES NOT LIKE LOCALHOST
// I never figured out why. Please let me know if you do!

describe('Testing Password Validation', function() {

    before(browser => browser
        .url('https://d2cgkfgsfyw731.cloudfront.net')
        .click(".V-Tab-Register")
        .setValue("input[id='input-48']", 'Apps')
        .setValue("input[id='input-51']", 'Dev')
        .setValue("input[id='input-54']", 'apps@test.com')

    );

    test('Do you get an error if you submit two different passwords', function(browser) {
        browser
        // This input field is password1
            .setValue("input[id='input-57']", 'inSecurePassword1')
            // This input field is password2
            .setValue("input[id='input-61']", 'inSecurePassword2')
            // Clicking 'Register'
            .click('button[class="register v-btn v-btn--is-elevated v-btn--has-bg theme--light v-size--default teal lighten-2"]')
            .assert.containsText('.displayErrors', 'Password input must match');
    });

    after(browser => browser.end());

});