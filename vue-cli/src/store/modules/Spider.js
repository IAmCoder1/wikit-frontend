import axios from "axios";
export default {
  namespaced: true,
  state: {
    externalScripts: {
      "https://www.flickr.com/": {
        scriptUrl: "https://embedr.flickr.com/assets/client-code.js",
        loaded: false,
      },
      "https://www.reddit.com/": {
        scriptUrl: "https://embed.redditmedia.com/widgets/platform.js",
        loaded: false,
      },
      "https://www.tiktok.com": {
        scriptUrl: "https://www.tiktok.com/embed.js",
        loaded: false,
      },
      "https://twitter.com": {
        scriptUrl: "https://platform.twitter.com/widgets.js",
        loaded: false,
      },
    },
    fetchCounter: 0,
    UserChoice: null,
    OGData: {},
    fetchStates: {},
    emptyCard: {
      url: null,
      data: {
        found: null,
        source: null,
        gettime: null,
        urldata: {
          quick_data: {},
          fetch_data: {},
          oembed_data: {},
        },
      },
    },
  },
  getters: {
    GET_OG_DATA: (state) => (url) => {
      return state.OGData[url] ? state.OGData[url] : state.emptyCard;
    },
    GET_FETCH_COUNTER: (state) => {
      return state.fetchCounter;
    },
    GET_ALL_OG_DATA: (state) => {
      return state.OGData;
    },
    GET_ALL_OG_STATES: (state) => {
      return state.fetchStates;
    },

    GET_OG_FETCH_STATE: (state) => (url) => {
      return state.fetchStates[url] ? state.fetchStates[url] : "idle";
    },
    GET_SPIDER_SCRIPTS: (state) => {
      return state.externalScripts;
    },
  },
  mutations: {
    SET_USER_CHOICE(state, payload) {
      state.UserChoice = payload;
    },
    SET_STATE_IS_FETCHING(state, payload) {
      state.fetchStates[payload] = "fetching";
    },
    SET_STATE_IS_FETCHED_COMPLETE(state, payload) {
      state.fetchStates[payload] = "fetch_complete";
    },
    SET_STATE_IS_FETCH_ERROR(state, payload) {
      state.fetchStates[payload] = "error";
    },
    PUSH_OG_DATA(state, payload) {
      state.fetchCounter++;
      let newdata;
      if (!state.OGData[payload.url]) {
        state.OGData[payload.url] = payload;
        return;
      } else {
        newdata = state.OGData[payload.url];
      }
      let c;
      let check = ["quick_data", "fetch_data", "oembed_data"];
      for (c in check) {
        if (payload.data?.urldata?.[check[c]]) {
          newdata.data.source = check[c].split("_")[0];
          newdata.data.urldata[check[c]] = payload.data.urldata[check[c]];
        }
      }
      state.OGData[payload.url] = newdata;
    },
    FLUSH_OG_DATA(state) {
      state.OGData = {};
    },
    SCRIPTS_LOADED_TRUE(state, payload) {
      state.externalScripts[payload].loaded = true;
    },
  },
  actions: {
    async CHOOSE_OG_CARD({ commit }, payload) {
      commit("SET_USER_CHOICE", payload);
    },
    async ADD_EXTERNAL_SCRIPTS({ commit }, payload) {
      commit("SCRIPTS_LOADED_TRUE", payload);
    },

    async FETCH_URL({ commit, state }, urlPayload) {
      // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      // console.log("This is the urlPayload and we hit it")
      // console.log(urlPayload)
      // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      let url =
        process.env.VUE_APP_SPIDERURL +
        "getfetchoembed/" +
        encodeURIComponent(urlPayload);
        // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        // console.log("This is the url endpoint")
        // console.log(url)
        // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      if (!state.OGData?.[urlPayload]) {
        commit("SET_STATE_IS_FETCHING", urlPayload);
        await axios
          .get(url, { url: urlPayload })
          .then(function (response) {
            commit("SET_STATE_IS_FETCHED_COMPLETE", urlPayload);

            commit("PUSH_OG_DATA", {
              url: urlPayload,
              data: response.data,
            });
          })
          .catch(function (error) {
            console.log(error);
            commit("SET_STATE_IS_FETCH_ERROR", urlPayload);
          });
      }
    },
    async FETCH_URL_QUICK({ commit, state }, urlPayload) {
      console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      console.log("This is the urlPayload and we hit it")
      console.log(urlPayload)
      console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      let url =
        process.env.VUE_APP_SPIDERURL +
        "getquick/" +
        encodeURIComponent(urlPayload);
      console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      console.log("This is the url endpoint")
      console.log(url)
      console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      if (!state.OGData?.[urlPayload]) {
        commit("SET_STATE_IS_FETCHING", urlPayload);
        await axios
          .get(url, { url: urlPayload })
          .then(function (response) {
            commit("SET_STATE_IS_FETCHED_COMPLETE", urlPayload);

            commit("PUSH_OG_DATA", {
              url: urlPayload,
              data: response.data,
            });
          })
          .catch(function (error) {
            console.log({ quick_data_error: error });
          });
      }
    },
  },
};
