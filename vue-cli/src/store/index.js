import Vue from "vue";
import Vuex from "vuex";

import APIClientStore from "./modules/APIClients";
import BlockUser from "./modules/BlockUser";
import CaptchaStore from "./modules/Captcha";
import ChangeStore from "./modules/Change";
import CommentStore from "./modules/Comment";
import Contact from "./modules/Contact";
import EditCopy from "./modules/EditCopy";
import EditStore from "./modules/Edit";
import FollowStore from "./modules/Follow";
import InboxStore from "./modules/Inbox";
import InterfaceStore from "./modules/Interface";
import Languages from "./modules/Language";
import MediaStore from "./modules/Media";
import ModerationStore from "./modules/Moderation";
import Onboarding from "./modules/Onboarding";
import PaypalStore from "./modules/Paypal";
import PostStore from "./modules/Post";
import PreferenceStore from "./modules/Preferences";
import RevisionStore from "./modules/Revision";
import SocketStore from "./modules/Sockets";
import SpiderStore from "./modules/Spider";
import StripeStore from "./modules/Stripe";
import SubWikiStore from "./modules/SubWiki";
import TrustStore from "./modules/Trust";
import UserContributionsStore from "./modules/UserContributions";
import UserManagementStore from "./modules/UserManagement";
import UserProfileStore from "./modules/UserProfile";
import UserStore from "./modules/User";
import VoteStore from "./modules/Vote";
import WYSIWYGStore from "./modules/WYSIWYG";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    APIClientStore,
    BlockUser,
    CaptchaStore,
    ChangeStore,
    CommentStore,
    Contact,
    EditCopy,
    EditStore,
    FollowStore,
    InboxStore,
    InterfaceStore,
    Languages,
    MediaStore,
    ModerationStore,
    Onboarding,
    PaypalStore,
    PostStore,
    PreferenceStore,
    RevisionStore,
    SocketStore,
    SpiderStore,
    StripeStore,
    SubWikiStore,
    TrustStore,
    UserContributionsStore,
    UserManagementStore,
    UserProfileStore,
    UserStore,
    VoteStore,
    WYSIWYGStore,
  },
  plugins: [],
});
