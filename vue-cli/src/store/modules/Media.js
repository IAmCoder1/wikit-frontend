import axios from "axios";
import Vue from "vue";

export default {
  namespaced: true,
  state: {
    blob: {},
    croppaData: {},
    pickedFiles: {},
    s3SignedUrl: {},
    uploadedS3Url: {},
    uploadState: {},
  },
  getters: {
    GET_S3_SIGNED_URL: (state) => {
      return state.s3SignedUrl;
    },
    GET_CROPPA_DATA: (state) => {
      return state.croppaData;
    },
    GET_PICKED_FILES: (state) => {
      return state.pickedFiles;
    },
    GET_UPLOADED_S3_URL: (state) => {
      return state.uploadedS3Url;
    },
    GET_UPLOAD_STATE: (state) => {
      return state.uploadState;
    },
  },
  mutations: {
    RESET_UPLOAD(state) {
      state.pickedFiles = {};
      state.s3SignedUrl = {};
      state.uploadedS3Url = {};
      state.uploadState = {};
    },
    SET_PICKED_FILE(state, payload) {
      Vue.set(state.pickedFiles, payload.purpose, payload.file);
    },
    SET_CROPPA_DATA(state, payload) {
      Vue.set(state.croppaData, payload.purpose, payload.croppaData);
    },
    SET_BLOB(state, payload) {
      Vue.set(state.blob, payload.purpose, payload.blob);
    },
    SET_S3_SIGNED_URL(state, payload) {
      Vue.set(state.s3SignedUrl, payload.purpose, payload.url);
    },
    SET_UPLOADED_S3_URL(state, payload) {
      Vue.set(state.uploadedS3Url, payload.purpose, payload.url);
    },
    SET_UPLOAD_STATE(state, payload) {
      Vue.set(state.uploadState, payload.purpose, payload.state);
    },
  },
  actions: {
    RESET_UPLOAD({ commit }, payload) {
      commit("RESET_UPLOAD", payload);
    },
    SET_PICKED_FILE({ commit }, payload) {
      commit("SET_PICKED_FILE", payload);
    },
    SET_CROPPA_DATA({ commit }, payload) {
      commit("SET_CROPPA_DATA", payload);
    },
    SET_MEDIA_BLOB({ commit }, payload) {
      commit("SET_BLOB", {
        blob: payload.blob,
        purpose: payload.purpose,
      });

    },
    async UPDATE_MY_PROFILE_PICTURE({commit, rootGetters}, srckey) {
      console.log("UPDATE_PROFILE_PICTURE");
      console.log("UPDATING");
      var url = process.env.VUE_APP_APIBASEURL + "media/makeprofilepic";
      var datatosend = { srckey: srckey };
      console.log({url, datatosend});
      axios
        .post(url, datatosend, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);
          commit('UserProfileStore/SET_MY_PROFILE_LAST_EDIT', { time: new Date(), what: 'profilePic' }, { root: true })
        })
        .catch(function (error) {
          console.log(error);
        });
    },

    INIT_UPLOAD({ commit, dispatch, state }, purpose) {
      if (!state.croppaData[purpose]) {
        console.log("No croppa data for purpose")
        console.log(state.croppaData);
        console.log(state.croppaData[purpose]);
        console.log(state.croppaData.coverPic);
        return false;
      }
      commit("SET_UPLOAD_STATE", { purpose: purpose, state: "generatingblob" });
      state.croppaData[purpose].generateBlob(
        (blob) => {
          // write code to upload the cropped image file (a file is a blob)
          const file = state.croppaData[purpose].getChosenFile();
          if (file?.name) {
            commit("SET_UPLOAD_STATE", {
              purpose: purpose,
              state: "settingblob",
            });

            commit("SET_BLOB", {
              blob: blob,
              purpose: purpose,
            });

            commit("SET_UPLOAD_STATE", {
              purpose: purpose,
              state: "fetchingurl",
            });
  
            let cfile = {
              purpose: purpose,
              name: file.name,
              type: "image/jpeg",
            };
            dispatch("FETCH_SIGNED_URL", cfile);
          } else {
            console.log(`${purpose} has no file`)
          }
        },
        "image/jpeg",
        0.8
      ); // 80% compressed jpeg file
    },
    SET_UPLOAD_STATE({ commit }, payload) {
      console.log("SET_UPLOAD_STATE");
      commit("SET_UPLOAD_STATE", payload);
    },

    async FETCH_SIGNED_URL({ commit, rootGetters, dispatch }, payload = null) {
      let url = process.env.VUE_APP_APIBASEURL + "media/getsignedurl";
      console.log(url);
      console.log(payload.purpose);

      let datatopost = {
        filePath: payload?.name,
        contentType: payload?.type,
      };
      axios
        .post(url, datatopost, rootGetters["UserStore/authHeader"])
        .then(async function (response) {
          console.log("Got signed URL: " + response.data.signedurl);
          commit("SET_S3_SIGNED_URL", {
            purpose: payload.purpose,
            url: response.data,
          });
          commit("SET_UPLOAD_STATE", {
            purpose: payload?.purpose,
            state: "uploading",
          });
          dispatch("UPLOAD_MEDIA", payload?.purpose);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    UPLOAD_MEDIA({ state, commit}, purpose) {
      if (!state.s3SignedUrl[purpose]) {
        console.log("No url for purpose: " + purpose);
      } else if (state.s3SignedUrl[purpose].signedurl && state.blob[purpose]) {
        axios
          .put(state.s3SignedUrl[purpose].signedurl, state.blob[purpose], {
            headers: { "cache-control": "no-cache", "content-type": state.blob[purpose].type },
          })
          .then(function () {
            
            commit("SET_UPLOAD_STATE", { purpose: purpose, state: "complete" });
            commit("SET_UPLOADED_S3_URL", { purpose: purpose, url: state.s3SignedUrl[purpose] });
            // switch (purpose) {
            //   case 'profilePic':
            //     dispatch('UserProfileStore/UPDATE_MY_PROFILE_PICTURE',  state.s3SignedUrl[purpose].s3key, {root: true});
        
            //     break;
            
            //   case 'coverPic':
            //     dispatch('UserProfileStore/UPDATE_MY_COVER_PICTURE',  state.s3SignedUrl[purpose].s3key, {root: true});
        
            //     break;
            
            //   default:
            //     break;
            // }

            commit("SET_S3_SIGNED_URL", { purpose: purpose, url: "" });
          })
          .catch(function (error) {
            console.log(error);
          });
      } else if (!state.s3SignedUrl[purpose].signedurl) {
        console.log("No url ");
      } else if (!state.blob[purpose]) {
        console.log("No blob ");
      }
    },
  },
};
