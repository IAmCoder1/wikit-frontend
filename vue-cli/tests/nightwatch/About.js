// This file mostly exists just to ensure that nightwatch is running.
// There's no reason we need to KNOW the About page has Simon's test
// data on it. Just playing around

// Nightwatch DOES NOT LIKE LOCALHOST
// I never figured out why. Please let me know if you do!
// before(browser => browser.url('https://localhost:4444/about'));

describe('Testing the About Page contents for WTS2.0', function() {

    before(browser => browser.url('https://d2cgkfgsfyw731.cloudfront.net/about'));

    test('Does that About Page header have the content we expect?', function(browser) {
        browser
            .assert.elementPresent("h1")
            .assert.containsText('h1', 'WTS2 (WT.Social MK-I)')
            .assert.elementPresent(".about")
            .assert.containsText('.about', 'Hello?\nThis is exciting');
    });

    after(browser => browser.end());
});

//.waitForElementVisible('app-content', 10000000)
// .assert.cssClassPresent("#about")
//.expect.element('#about').to.be.present;
// browser.assert.cssClassPresent("#main", "container");
// .assert.titleContains('Ecosia')
// .assert.visible('input[type=search]')
// .setValue('input[type=search]', 'nightwatch')
// .assert.visible('button[type=submit]')
// .click('button[type=submit]')