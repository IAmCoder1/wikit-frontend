aws dynamodb delete-item \
    --table-name WTS2AlphaAuth \
    --key file://delete.json \
    --return-values ALL_OLD \
    --return-consumed-capacity TOTAL \
    --return-item-collection-metrics SIZE