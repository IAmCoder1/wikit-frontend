
export default {
    namespaced: true,
    state: {
        state: "waiting",
        token: null
    },
    actions: {
        CAPTCHA_RESET({ commit }) {
            commit("SET_CAPTCHA_WAITING");
            commit("SET_CAPTCHA_TOKEN", null);
        },
        CAPTCHA_CHECKING({ commit }) {
            commit("SET_CAPTCHA_CHECKING");
        },
        CAPTCHA_BOT_DETECTED({ commit }) {
            commit("SET_CAPTCHA_BOT");
        },
        CAPTCHA_APPROVED({ commit }, tokenPayload) {
            commit("SET_CAPTCHA_APPROVED");
            commit("SET_CAPTCHA_TOKEN", tokenPayload);
        },
        CHECK_CAPTCHA({ dispatch }) {
            if (window.grecaptcha) {
              window.grecaptcha.ready(function () {
                  
                dispatch('CAPTCHA_CHECKING');
                window.grecaptcha
                  .execute(process.env.VUE_APP_GOOGLE_CAPTCHA_SITE_KEY, {
                    action: self.action,
                  })
                  .then(function (token) {
                    // Add your logic to submit to your backend server here.
                    dispatch('CAPTCHA_APPROVED', token);
                  });
              });
            } else {
              console.log("window.grecaptcha not found");
            }
          },
       
    },
    mutations: {
   
        SET_CAPTCHA_TOKEN(state, tokenPayload) {
            state.token = tokenPayload
        },
        SET_CAPTCHA_WAITING(state) {
            state.state = "waiting";
        },
        SET_CAPTCHA_APPROVED(state) {
            state.state = "approved";
        },
        SET_CAPTCHA_BOT(state) {
            state.state = "botdetected";
        },
        SET_CAPTCHA_CHECKING(state) {
            state.state = "checking";
        },
    },
    getters: {
       
        GET_CAPTCHA_TOKEN(state) {
            return state.token;
        },
       
        GET_CAPTCHA_STATE(state) {
            return state.state;
        }
    },
};