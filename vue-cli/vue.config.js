const path = require("path");
module.exports = {
  devServer: {
    host: 'localhost',

  },
  // publicPath: "/",
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
          @import "@/assets/scss/variables.scss";
          @import "@/assets/scss/main.scss";        
        `,
      },
    },
  },
  transpileDependencies: ["vuetify"],
  pages: {
    index: {
      // entry for the page
      entry: "src/main.js",
      title: "A non-toxic social experience",
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias.set(
      "@shared",
      path.resolve(__dirname, "../vue-cli-shared/")
    );
  },
};
